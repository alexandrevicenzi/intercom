import tempfile
import unittest

import main


class TestMain(unittest.TestCase):
    def test_get_invite_list(self):
        input_data = [
            (
                [
                    {
                        "latitude": "52.240382",
                        "user_id": 1,
                        "name": "Foo",
                        "longitude": "-6.972413",
                    },
                    {
                        "latitude": "53.2451022",
                        "user_id": 2,
                        "name": "Bar",
                        "longitude": "-6.238335",
                    },
                ],
                [{"user_id": 2, "name": "Bar"}],
            ),
            ([], []),
            (
                [
                    {
                        "latitude": "52.240382",
                        "user_id": 1,
                        "name": "Foo",
                        "longitude": "-6.972413",
                    },
                    {
                        "latitude": "52.2451022",
                        "user_id": 2,
                        "name": "Bar",
                        "longitude": "-6.238335",
                    },
                ],
                [],
            ),
        ]

        for customers, expected in input_data:
            with self.subTest():
                actual = main.get_invite_list(customers, 53.339428, -6.257664, 100)
                self.assertEqual(actual, expected)

    def test_read_input(self):
        expected = [{"latitude": "0", "user_id": 0, "name": "Bar", "longitude": "0",}]

        with tempfile.NamedTemporaryFile() as fp:
            fp.write(
                b'{"latitude": "0", "user_id": 0, "name": "Bar", "longitude": "0"}'
            )
            fp.seek(0)

            actual = main.read_input([fp.name])

            self.assertEqual(
                actual, expected,
            )

    def test_read_input_invalid(self):
        with tempfile.NamedTemporaryFile() as fp:
            fp.write(b'{"latitude": xxxxx')
            fp.seek(0)

            actual = main.read_input([fp.name])

            self.assertEqual(
                actual, [],
            )

    def test_read_input_does_not_exist(self):
        actual = main.read_input(["/invalid-file-name"])
        expected = []

        self.assertEqual(
            actual, expected,
        )

    def test_write_output(self):
        data = {"test": True}

        with tempfile.NamedTemporaryFile() as fp:
            main.write_output(data, fp.name)
            fp.seek(0)
            text = fp.read().decode()
            self.assertEqual(text, '{\n    "test": true\n}')

    def test_gcd(self):
        input_data = [
            (-34.83333, -58.5166646, 49.0083899664, 2.53844117956, 11099.540355819661),
            (49.0083899664, 2.53844117956, -34.83333, -58.5166646, 11099.540355819661),
            (48.1372, 11.5756, 52.5186, 13.4083, 504.2157151825301),
            (52.5186, 13.4083, 48.1372, 11.5756, 504.2157151825301),
            (52.5186, 13.4083, 52.5186, 13.4083, 0),
        ]

        for lat1, lon1, lat2, lon2, expected in input_data:
            with self.subTest():
                actual = main.gcd(lat1, lon1, lat2, lon2)
                self.assertEqual(actual, expected)


if __name__ == "__main__":
    unittest.main()
