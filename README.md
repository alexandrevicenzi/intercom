# Intercom Test

Thanks for reviewing this test.

The project is buit in Python 3 and no external libraries are needed.

Code is formated using Black.

There was no output format specified in the task, so, as the input is JSON, the output is also JSON.

## Usage

Use the following command to run the script.

`python3 main.py --input customers.txt --output output.txt --latitude 53.339428 --longitude -6.257664 --distance 100`

It's also possible to check CLI args with the following command.

`python3 main.py -h`

## Test

Use the following command to test the script.

`python3 tests.py -v`
