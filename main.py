import argparse
import json
import logging
import os
import sys

from math import radians, sin, cos, acos

EARTH_RADIUS = 6371
LOG = logging.getLogger(__name__)


def parse_args():
    """ Parses comand line arguments. """
    parser = argparse.ArgumentParser(description="Intercom Test")
    parser.add_argument(
        "--output",
        metavar="FILE",
        type=str,
        default=None,
        required=True,
        help="output file",
    )
    parser.add_argument(
        "--input",
        metavar="FILE",
        nargs="*",
        type=str,
        default=None,
        required=True,
        help="input files",
    )
    parser.add_argument(
        "--latitude",
        metavar="LAT",
        type=float,
        default=None,
        required=True,
        help="latitude of location",
    )
    parser.add_argument(
        "--longitude",
        metavar="LONG",
        type=float,
        default=None,
        required=True,
        help="longitude of location",
    )
    parser.add_argument(
        "--distance",
        metavar="D",
        type=float,
        default=None,
        required=True,
        help="distance in KM",
    )

    args = parser.parse_args()

    return args


def gcd(lat1, lon1, lat2, lon2):
    """ Calculate Great-circle distance """
    lat1, lon1, lat2, lon2 = map(radians, [lat1, lon1, lat2, lon2])
    return EARTH_RADIUS * (
        acos(sin(lat1) * sin(lat2) + cos(lat1) * cos(lat2) * cos(lon1 - lon2))
    )


def read_input(input_files):
    """ Reads multiple input files as a list of dictionaries. """
    data = []

    for file in input_files:
        LOG.info("loading file: %s", file)

        if not os.path.isfile(file):
            LOG.error("File does not exists: %s", file)
            continue

        with open(file, "r") as f:
            for line in f.readlines():
                try:
                    data.append(json.loads(line))
                except Exception as e:
                    LOG.error("Error parsing line: %s", str(e))
    return data


def write_output(data, output_file):
    """ Writes a python object to a file. """
    LOG.info("writing file: %s", output_file)

    with open(output_file, "w") as f:
        f.write(json.dumps(data, indent=4))


def get_invite_list(customers, latitude, longitude, distance):
    """
        Retuns a sorted list of customers that are close to the given location.

        Parameters:
            customers (list): customer list
            latitude (float): latitude of event location
            longitude (float): longitude of event location
            distance (float): max distance from location

        Returns:
            list: customers near event location
    """
    to_invite = []

    for c in customers:
        c_latitude = float(c.pop("latitude"))
        c_longitude = float(c.pop("longitude"))
        c_distance = gcd(latitude, longitude, c_latitude, c_longitude)

        if c_distance <= distance:
            to_invite.append(c)

    return sorted(to_invite, key=lambda item: item["user_id"])


def configure_log():
    """ Configure log output """
    logging.basicConfig(
        stream=sys.stdout,
        level=logging.INFO,
        format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
        datefmt="%Y-%m-%dT%H:%M:%S",
    )


def run(args):
    customer_list = read_input(args.input)
    customers_to_invite = get_invite_list(
        customer_list, args.latitude, args.longitude, args.distance
    )
    write_output(customers_to_invite, args.output)
    LOG.info("done")


if __name__ == "__main__":
    configure_log()
    args = parse_args()
    run(args)
